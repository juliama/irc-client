import threading
import select
import socket
import html
import re
from PyQt5 import QtGui
from PyQt5.QtGui import QRegExpValidator
from PyQt5.QtCore import QSize, pyqtSlot, pyqtSignal, QRegExp
from PyQt5.QtWidgets import QMainWindow, QTextBrowser
from PyQt5.uic import loadUi

receiving_queue = []
sending_queue = []


class MainWindow(QMainWindow):
    # qt
    new_message = pyqtSignal()
    quit = pyqtSignal()
    channels = []
    server_channel = None
    current_channel = None

    # network thread
    needs_restart = False
    server = ''
    port = 0
    server_address = (server, port)
    nick = ''

    # NETWORK-CONNECTIVITY---------------------------------------------------------------------- #

    # main network connection thread
    def socket_listener(self):
        global receiving_queue
        global sending_queue
        self.needs_restart = False
        end_of_message = '\r\n'
        last_two_bytes = [b'a', b'a']
        message = ''

        # TCP/IP
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.connect(self.server_address)
        except Exception as e:
            print(e.__class__.__name__ + " while trying to connect to the server")
            self.quit.emit()
            return

        sending_queue.append(f'NICK {self.nick}')

        # main read/write loop
        while True:
            if self.needs_restart:
                print('Connection closed')
                break

            try:
                ready_to_read, ready_to_write, in_error = select.select([sock, ], [sock, ], [])
            except select.error:
                print('Error in select')
                break

            try:
                # receive message and append to receiving_queue
                if len(ready_to_read) > 0:
                    data = sock.recv(1)
                    message += data.decode("utf-8")

                    # detect end of message
                    last_two_bytes[1] = last_two_bytes[0]
                    last_two_bytes[0] = data
                    if b'\n' in last_two_bytes[0] and b'\r' in last_two_bytes[1]:
                        receiving_queue.append(message[:-len(end_of_message)])
                        self.new_message.emit()
                        message = ''
            except UnicodeDecodeError:
                pass
            except ConnectionAbortedError:
                print("Connection aborted.")
                break
            except Exception as e:
                print(e.__class__.__name__ + " while trying to read message from server")
                break

            try:
                # send message from sending_queue
                if len(ready_to_write) > 0:
                    m = sending_queue.pop(0)
                    sock.sendall(str.encode(m + end_of_message))

                    # check if quit message was sent
                    quit_regexp = r'^QUIT :(.{0,128})'
                    quit_result = re.match(quit_regexp, m)
                    if bool(quit_result):
                        print(f"Quit server with leave message \"{quit_result.groups()[0]}\"")
                        self.quit.emit()
            except IndexError:
                pass
            except Exception as e:
                print(e.__class__.__name__ + " while trying to send message to server")
                break

        self.notify_about_disconnect()
        # close socket
        sock.close()

    # main thread starter
    def start_socket_listener(self):
        global receiving_queue
        global sending_queue
        receiving_queue = []
        sending_queue = []
        print('Starting network connection.')
        t = threading.Thread(target=self.socket_listener)
        t.setDaemon(True)
        t.start()
        t.join()
        receiving_queue = []
        sending_queue = []

    # thread runner necessary so that join can be used in start_socket_listener
    # this way connections can be reset safely
    def start_network_connectivity(self):
        # start TCP/IP connection
        runner = threading.Thread(target=self.start_socket_listener)
        runner.setDaemon(True)
        runner.start()

    # called by user (through button click or enter) to connect to a server of choice
    @pyqtSlot()
    def connect_to_server(self):
        # read user input form GUI
        self.server = self.lineEdit_2.text()
        self.port = int(self.lineEdit_3.text())
        self.server_address = (self.server, self.port)
        self.nick = self.lineEdit_4.text()

        # set up server channel info
        self.server_channel = [self.server, self.textBrowser, 'Communication with server', []]

        # set current channel to the server channel
        self.current_channel = self.server_channel
        self.clear_channel(self.current_channel[0])
        self.current_channel[1].setVisible(True)

        # GUI setup
        self.set_main_gui()
        self.label_2.setText(self.nick)
        self.label_7.setText(self.current_channel[2])
        self.comboBox.addItem(self.server)

        # start network connection
        self.start_network_connectivity()

    # close connection to server and set the 'login' GUI
    @pyqtSlot()
    def disconnect_from_server(self):
        # tell network thread it's time to stop
        self.needs_restart = True

        # setup login GUI
        self.set_secondary_gui()

        # clear GUI
        self.label_2.setText('')
        self.comboBox.clear()
        self.channels = []
        self.current_channel[1].setVisible(False)
        self.clear_channel(self.server_channel[0])

    # NETWORK-CONNECTIVITY---------------------------------------------------------------------- #
    #                                                                                            #
    # GUI--------------------------------------------------------------------------------------- #

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        # load .ui file
        self.ui = loadUi('irc.ui', self)
        self.setWindowTitle('IRC Client')
        self.setFixedSize(QSize(680, 600))

        # initial values for lineEdits
        self.lineEdit_2.setText('192.168.8.103')
        self.lineEdit_3.setText('6667')
        self.lineEdit_4.setText('julia')
        self.label_2.setText('')

        # user input comboBox settings
        self.comboBox_2.setInsertPolicy(1)
        self.comboBox_2.setMaxCount(500)

        # regexp validators
        self.comboBox_2.setValidator(QRegExpValidator(QRegExp(r'[\x00-\x7F]{1,128}')))
        self.lineEdit_2.setValidator(QRegExpValidator(QRegExp(r'[\x00-\x7F]{1,128}')))
        self.lineEdit_3.setValidator(QRegExpValidator(QRegExp(r'[0-9]{1,5}')))
        self.lineEdit_4.setValidator(QRegExpValidator(QRegExp(r'[a-zA-Z0-9_~]{1,12}')))

        # signal to slot
        self.new_message.connect(self.process_message)
        self.quit.connect(self.disconnect_from_server)

        self.pushButton.clicked.connect(self.get_user_input)
        self.pushButton_2.clicked.connect(self.connect_to_server)
        self.pushButton_3.clicked.connect(self.quit)
        self.pushButton_4.clicked.connect(self.clear_channel)

        self.comboBox.currentTextChanged.connect(self.change_channel)

        self.lineEdit_2.textChanged.connect(self.update_button_status)
        self.lineEdit_3.textChanged.connect(self.update_button_status)
        self.lineEdit_4.textChanged.connect(self.update_button_status)

        # group GUI parts
        self.secondary_gui_elements = [self.lineEdit_2, self.lineEdit_3, self.lineEdit_4,
                                       self.label_3, self.label_4, self.label_5,
                                       self.pushButton_2]
        self.main_gui_elements = [self.comboBox_2, self.pushButton, self.label, self.label_2,
                                  self.comboBox, self.pushButton_3, self.pushButton_4,
                                  self.label_6, self.label_7]

        self.textBrowser.setVisible(False)

        self.set_secondary_gui()

    # login GUI
    def set_secondary_gui(self):
        for element in self.main_gui_elements:
            element.setVisible(False)
        for element in self.secondary_gui_elements:
            element.setVisible(True)

    # IRC GUI
    def set_main_gui(self):
        for element in self.main_gui_elements:
            element.setVisible(True)
        for element in self.secondary_gui_elements:
            element.setVisible(False)

    # ENTER press gets user input or connects to server
    def keyPressEvent(self, event):
        if type(event) == QtGui.QKeyEvent:
            if event.key() == 16777220:
                if self.comboBox_2.isVisible():
                    self.get_user_input()
                elif self.pushButton_2.isVisible() and self.pushButton_2.isEnabled():

                    self.connect_to_server()
                event.accept()
        else:
            event.ignore()

    @pyqtSlot(str)
    def update_button_status(self, _):
        s = self.lineEdit_2.text().strip()
        p = self.lineEdit_3.text().strip()
        n = self.lineEdit_4.text().strip()
        conditions = [s != '', p != '', n != '']
        if all(conditions):
            self.pushButton_2.setEnabled(True)
        else:
            self.pushButton_2.setEnabled(False)

    def notify_about_disconnect(self):
        self.needs_restart = True
        all_channels = self.channels + [self.server_channel]
        for ch in all_channels:
            ch[1].append(f'<html><b>+++ You have been disconnected form the server. '
                         f'Press "Leave server" to try connecting again.</b></html>')

    # GUI--------------------------------------------------------------------------------------- #
    #                                                                                            #
    # CHANNELS---------------------------------------------------------------------------------- #

    # create channel
    def create_channel(self, name):
        # create textBrowser
        textbox = QTextBrowser(self)
        textbox.setGeometry(20, 80, 631, 431)
        textbox.setVisible(False)

        # set a default topic
        topic = '-' if name[0] == '#' else 'Private conversation'

        # list of users in channel
        users = []

        # add to channel list
        self.channels.append([name, textbox, topic, users])

        # update comboBox
        self.comboBox.addItem(name)

    # delete channel
    def delete_channel(self, name):
        # remove channel from channel list
        self.channels = [ch for ch in self.channels if ch[0] != name]

        # remove channel name from comboBox
        self.comboBox.removeItem(self.comboBox.findText(name))

    # change channel
    @pyqtSlot(str)
    def change_channel(self, name):
        if name in ['']:
            return
        self.current_channel[1].setVisible(False)

        if name == self.server:
            self.current_channel = self.server_channel
        else:
            for ch in self.channels:
                if ch[0] == name:
                    self.current_channel = ch

        self.label_7.setText(self.current_channel[2])

        self.current_channel[1].setVisible(True)

    # clear text from textBrowser
    @pyqtSlot()
    def clear_channel(self, name=-1):
        if name == -1:
            self.current_channel[1].clear()
        else:
            if name == self.server:
                self.server_channel[1].clear()

    # CHANNELS---------------------------------------------------------------------------------- #
    #                                                                                            #
    # MESSAGES---------------------------------------------------------------------------------- #

    # read input from user from GUI and add it to the sending queue
    @pyqtSlot()
    def get_user_input(self):
        global sending_queue
        user_input = self.comboBox_2.currentText()

        # clear combobox if full
        if self.comboBox_2.count() == self.comboBox_2.maxCount():
            self.comboBox_2.clear()

        # ignore user input if empty
        if len(user_input) == 0:
            return

        # interpret input as command
        if user_input[0] == r'/':
            final_message = user_input[1:]

            # check if command is PRIVMSG to user
            privmsg_regexp = r'^(PRIVMSG) ([a-zA-Z0-9_~]{1,12}) :(.{0,128})|' \
                             r'^(MSG) ([a-zA-Z0-9_~]{1,12}) (.{0,128})'
            privmsg_result = re.match(privmsg_regexp, final_message)

            # check if command is NAMES
            names_regexp = r'^NAMES ((#[a-zA-Z]{1,11}))'
            names_result = re.match(names_regexp, final_message)

            if bool(privmsg_result):
                if privmsg_result.groups()[0] == 'PRIVMSG':
                    target = privmsg_result.groups()[1]
                    text = privmsg_result.groups()[2]
                else:
                    target = privmsg_result.groups()[4]
                    text = privmsg_result.groups()[5]
                    final_message = f'PRIVMSG {target} :{text}'

                already_exists = False
                for ch in self.channels:
                    if ch[0] == target:
                        already_exists = True

                # create channel if doesn't exist
                if not already_exists:
                    self.create_channel(target)

                # show message in private channel
                for ch in self.channels:
                    if ch[0] == target:
                        ch[1].append(f'<html>&lt;<b>{self.nick}</b>&gt;</html> ' +
                                     html.escape(text, quote=True))
                        self.comboBox.setCurrentIndex(self.comboBox.findText(ch[0]))

            elif bool(names_result):
                channel = names_result.groups()[0]
                # clear list name
                for ch in self.channels:
                    if ch[0] == channel:
                        ch[3] = []

        # interpret input as privmsg to current channel
        else:
            if self.current_channel[0] == self.server:
                self.comboBox_2.clearEditText()
                return
            final_message = f'PRIVMSG {self.current_channel[0]} :{user_input}'
            self.current_channel[1].append(f'<html>&lt;<b>{self.nick}</b>&gt;</html> ' +
                                           html.escape(user_input, quote=True))

        sending_queue.append(final_message)
        self.server_channel[1].append(f"+ Sent message \"{final_message}\"")
        self.comboBox_2.clearEditText()

    # processing and possibly responding to RECEIVED messages
    @pyqtSlot()
    def process_message(self):
        global receiving_queue
        global sending_queue
        # if there's no message to be processed return
        try:
            message = str(receiving_queue.pop(0))
        except IndexError:
            return

        # show message in the server channel
        self.server_channel[1].append(f"+ Received message \"{message}\"")

        # PING
        ping_regexp = r'^PING :(\S+)$'
        ping_result = re.match(ping_regexp, message)

        # NICK
        nick_regexp = r'^:([a-zA-Z0-9_~]{1,12})!.* NICK :([a-zA-Z0-9_~]{1,12})$'
        nick_result = re.match(nick_regexp, message)

        # JOIN
        join_regexp = r'^:([a-zA-Z0-9_~]{1,12})!.* JOIN :(#[a-zA-Z]{1,11})$'
        join_result = re.match(join_regexp, message)

        # PART
        part_regexp = r'^:([a-zA-Z0-9_~]{1,12})!.* PART (#[a-zA-Z]{1,11})$'
        part_result = re.match(part_regexp, message)

        # PRIVMSG
        privmsg_regexp = r'^:([a-zA-Z0-9_~]{1,12})!.* ' \
                         r'PRIVMSG ([a-zA-Z0-9_~]{1,12}|#[a-zA-Z]{1,11}) :(.{0,128})'
        privmsg_result = re.match(privmsg_regexp, message)

        # TOPIC
        topic_regexp = r'^:([a-zA-Z0-9_~]{1,12})!.* TOPIC (#[a-zA-Z]{1,11}) :(.{0,128})'
        topic_result = re.match(topic_regexp, message)

        # NAMES
        names_353_regexp = r'^:.* (\d{3}) [a-zA-Z0-9_~]{1,12} = (#[a-zA-Z]{1,11}) :([a-zA-Z0-9_~]{1,12})'
        names_353_result = re.match(names_353_regexp, message)

        names_366_regexp = r'^:.* (\d{3}) [a-zA-Z0-9_~]{1,12} (#[a-zA-Z]{1,11}) :.*'
        names_366_result = re.match(names_366_regexp, message)

        if bool(ping_result):
            ping = ping_result.groups()[0]
            pong = f":{ping} PONG :{ping}"
            sending_queue.append(pong)

        elif bool(nick_result):
            old_nick = nick_result.groups()[0]
            new_nick = nick_result.groups()[1]
            self.nick = new_nick
            self.label_2.setText(self.nick)
            self.server_channel[1].append(f"+ Your nick changed from "
                                          f"\"{old_nick}\" to \"{new_nick}\"")

        elif bool(join_result):
            user = join_result.groups()[0]
            channel = join_result.groups()[1]
            self.server_channel[1].append(f"+ {user} joined channel {channel}")

            if self.nick == user:
                self.create_channel(channel)
                self.comboBox.setCurrentIndex(self.comboBox.findText(channel))

            for ch in self.channels:
                if ch[0] == channel:
                    ch[1].append(f"+ <html><b>{user}</b></html> "
                                 f"joined channel <html><b>{channel}</b></html>")

        elif bool(part_result):
            user = part_result.groups()[0]
            channel = part_result.groups()[1]
            self.server_channel[1].append(f"+ {user} left channel {channel}")

            if self.nick == user:
                self.delete_channel(channel)
            else:
                for ch in self.channels:
                    if ch[0] == channel:
                        ch[1].append(f"+ <html><b>{user}</b> "
                                     f"left channel <b>{channel}</b></html>")

        elif bool(privmsg_result):
            user = privmsg_result.groups()[0]
            target = privmsg_result.groups()[1]
            text = privmsg_result.groups()[2]

            # message to user
            if target == self.nick:
                already_exists = False
                for ch in self.channels:
                    if ch[0] == user:
                        already_exists = True

                # create channel if doesn't exist
                if not already_exists:
                    self.create_channel(user)

                # show message in private channel
                for ch in self.channels:
                    if ch[0] == user:
                        ch[1].append(f"<html>&lt;<b>{user}</b>&gt;</html> " + html.escape(text, quote=True))

            # message to channel
            else:
                # show message in channel
                for ch in self.channels:
                    if ch[0] == target:
                        ch[1].append(f"<html>&lt;<b>{user}</b>&gt;</html> " + html.escape(text, quote=True))

        elif bool(topic_result):
            user = topic_result.groups()[0]
            channel = topic_result.groups()[1]
            topic = topic_result.groups()[2]
            self.server_channel[1].append(f"+ User {user} set channel {channel} "
                                          f"topic to \"{topic}\"")
            for ch in self.channels:
                if ch[0] == channel:
                    ch[2] = topic
                    ch[1].append(f"<html>+ Topic set to \"" + html.escape(topic, quote=True) +
                                 f"\" by <b>{user}</b></html>")

            # update label if topic changed in current channel
            if self.current_channel[0] == channel:
                self.label_7.setText(topic)

        elif bool(names_353_result):
            channel = names_353_result.groups()[1]
            name = names_353_result.groups()[2]

            # add name to list
            for ch in self.channels:
                if ch[0] == channel:
                    ch[3].append(name)

        elif bool(names_366_result):
            channel = names_366_result.groups()[1]

            #  print name list on channel
            for ch in self.channels:
                if ch[0] == channel:
                    ch[1].append(f"<html><b>+ Users in channel {channel}:</b></html>")
                    for n in ch[3]:
                        ch[1].append(f"<html><b>[" + html.escape(n, quote=True) + f"]</b></html>")

    # MESSAGES---------------------------------------------------------------------------------- #
